This repository contains a collection of machine learning assignments focused on sentiment analysis, text classification, and image classification. 

Each assignment includes detailed documentation, code, and evaluation metrics.

Thank you so much for your kind support, everyone!